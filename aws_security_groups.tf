# Security group for Load-Balancer

resource "aws_security_group" "load_balancer_sg" {
  vpc_id = aws_vpc.production_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "lb_sg"
  }
}


# Security group for JumpServer

resource "aws_security_group" "jumphost_sg" {
  name        = "jumphost_security_group"
  description = "Security Group for Jump Host"
  vpc_id      = aws_vpc.production_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    #cidr_blocks = [local.admin_ip_address] #32 mask indicate only one ip
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "jumphost_sg"
  }
}


# Security group for Private EC2 Instances

resource "aws_security_group" "private_instance_sg" {
  vpc_id = aws_vpc.production_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    #cidr_blocks = [local.jumphost_private_ip]
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Dla ruchu HTTP od load balancera
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.production_vpc.cidr_block]

  }

  # Dla ruchu HTTPS od load balancera
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.production_vpc.cidr_block]
  }

  # Dla całego ruchu wychodzącego
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "private_instance_sg"
  }
}