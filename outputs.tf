output "lb_dns_name" {
  description = "You can visit your instances thru load-balance (The DNS name of the LB):"
  value       = aws_lb.my_lb.dns_name
}

output "jump_host_public_ip" {
  value = aws_eip.one.public_ip
}

data "aws_instances" "my_private_instances" {
  instance_tags = {
    "aws:autoscaling:groupName" = aws_autoscaling_group.asg.name
  }
}

output "private_ips" {
  value = data.aws_instances.my_private_instances.private_ips
}

output "private_ips_output" {
  value = [for ip in data.aws_instances.my_private_instances.private_ips : "Connect to instance: ssh -J ubuntu@${aws_eip.one.public_ip} ubuntu@${ip}"]
}


