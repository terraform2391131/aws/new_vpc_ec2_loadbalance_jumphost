# New_VPC_EC2_Load_Balance_JumpHost
# What's included:
# Simple example how to create Load-balancer with two (default) instances.
# Without Jump Host! If you need to connect to one of instances:

# OPTIONS THAT DON'T! WORK (for me):
# ssh -J ubuntu@<jumphost> -o "IdentityFile=private_key.pem" -i ~/.ssh/private_key.pem ubuntu@<privatehostip>
# ssh -J ubuntu@<jumphost> -i "private_key.pem" ubuntu@<privatehostip>

# OPTION THAT WORK'S:
# ssh-add -L      -will output your ssh identity's, there should be only your one system identity
# ssh-add -D      -will clear all other identity's
# ssh-add /path/to/private-key    - like: ssh-add private_ket.pem
# ssh -J ubuntu@<jumphost> ubuntu@<privatehostip>
# 
# What's included:
# New VPC
# 2x EC2 instance with Ubuntu and apache2
# NAT Gateway for thoes two EC2 instances to talk to internet 
# Load-balancer (80 or 433)
# Load-balancer - Health check
# 2x Route Table, one for Load Balancer, one for EC2 instances
# Key value pairs for SSH 22
# Auto Scaling Group

# Whats need to be set in this project, file(terraform.tfvars):
aws_region = "eu-central-1"
aws_zone1 = "eu-central-1a"
aws_zone2 = "eu-central-1b"
project_short_name = "project1"

instances_image_id = "ami-061afb021813a8927"
instances_type = "t3.micro"

admin_ip_address = "84.59.108.131"

# this is learn project so lot's to set up in main.tf


# 1. Create vpc
# 2. Create Internet Gateway (to send trafic out)
# 3. Create Subnet's
# 4. Create NAT Gateway
# 5. Create Custom Route Table
# 6. Associate subnet with Route Table
# 7. Create key-value pair for port 22
# 8. Launch Template
# 9. Application Load Balancer
# 10. Auto Scaling Group