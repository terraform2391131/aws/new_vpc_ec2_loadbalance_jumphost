# New_VPC_EC2_Load_Balance_JumpHost
<br>
Simple example how to create Load-balancer with two (default) instances.<br>
With extra EC2 acting as Jump Host! If you need to connect to one of instances:<br>
<br>
OPTIONS THAT DON'T! WORK (for me):<br>
ssh -J ubuntu@<jumphost> -o "IdentityFile=private_key.pem" -i ~/.ssh/private_key.pem ubuntu@<privatehostip><br>
ssh -J ubuntu@<jumphost> -i "private_key.pem" ubuntu@<privatehostip><br>
<br>
OPTION THAT WORK'S:<br>
ssh-add -L      -will output your ssh identity's, there should be only your one system identity<br>
ssh-add -D      -will clear all other identity's<br>
ssh-add /path/to/private-key    - like: ssh-add private_ket.pem<br>
ssh -J ubuntu@<jumphost> ubuntu@<privatehostip><br>
<br>
What's included:<br>
<br>
New VPC<br>
1x EC2 instance JumpHost with Ubuntu and apache2<br>
2x EC2 instance with Ubuntu and apache2<br>
NAT Gateway for thoes two EC2 instances to talk to internet <br>
Load-balancer (80 or 433)<br>
Load-balancer - Health check<br>
2x Route Table, one for Load Balancer, one for EC2 instances<br>
Key value pairs for SSH 22<br>
Auto Scaling Group<br>
<br>
# Edit all settings only in "terraform.tfvars" file
